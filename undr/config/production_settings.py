from .settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '48g_)(%%=gq^%%6nonu0m8y%33&^!)yhq!brso)m$!g!%!bi_j'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dev',
        'USER': 'dev',
        'PASSWORD': 'qwe123qwe',
        'HOST': 'localhost',
        'PORT': '',
    }
}
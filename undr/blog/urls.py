from django.contrib import admin
from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    #path('', views.index, name="index"),
    path('', views.ViewMainPage.as_view(), name="index"),
    path('articles/<slug:slug>/', views.ViewPostPage.as_view(), name="post_detail"),
    path('<slug:category_slug>/', views.ViewCategoryPage.as_view(), name='category_detail'),
]
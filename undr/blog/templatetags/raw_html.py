from django import template

register = template.Library()

@register.filter(name='raw_html', is_safe=True)
def raw_html(value):
	return value

from django.contrib import admin
from .models import Post, Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ['author', 'title', 'text', 'created_date', 'published_date']
    prepopulated_fields = {'slug': ('title', )}

    fieldsets = (
        ('Information', {
            'fields': [('title', 'slug', 'author'),('announce', 'categories'), 'text', ('seo_title', 'seo_description',
                                                                                        'seo_keywords')]
        }),
        ('Other', {
            'fields': ('published_date', 'created_date')
        }),
    )


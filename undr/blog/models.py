from django.db import models
from django.urls import reverse
from django.utils import timezone

from ckeditor_uploader.fields import RichTextUploadingField

from seo.models import SeoModel


class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Categories'

    def get_absolute_url(self):
        return reverse('blog:category_detail', args=[self.slug])

    def __str__(self):
        return self.name


class Post(SeoModel):
    categories = models.ManyToManyField(Category, verbose_name="Категория")
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, max_length=50)
    title = models.CharField(max_length=200)
    announce = models.TextField()
    text = RichTextUploadingField(blank=True, default='')
    slug = models.SlugField(max_length=200, db_index=True)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    articles = 'articles'

    class Meta:
        ordering = ['published_date']
        index_together = [
            ['title', 'slug']
        ]

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[self.slug])

    def __str__(self):
        return self.title

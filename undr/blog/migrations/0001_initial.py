# Generated by Django 2.0.5 on 2018-12-02 16:29

import ckeditor_uploader.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.IntegerField(verbose_name='Цена')),
                ('old_price', models.IntegerField(verbose_name='Старая цена')),
                ('discount', models.IntegerField(verbose_name='Скидка')),
                ('title', models.CharField(max_length=250, verbose_name='Название')),
                ('platform', models.TextField(max_length=50, verbose_name='Платформа')),
                ('link', models.URLField(verbose_name='Ссылка')),
            ],
            options={
                'db_table': 'game',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('seo_title', models.CharField(blank=True, max_length=250, verbose_name='Title')),
                ('seo_description', models.CharField(blank=True, max_length=250, verbose_name='Description')),
                ('seo_keywords', models.CharField(blank=True, max_length=250, verbose_name='Keywords')),
                ('title', models.CharField(max_length=200)),
                ('announce', models.TextField()),
                ('text', ckeditor_uploader.fields.RichTextUploadingField(blank=True, default='')),
                ('slug', models.SlugField(max_length=200)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('published_date', models.DateTimeField(blank=True, null=True)),
                ('author', models.ForeignKey(max_length=50, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('categories', models.ManyToManyField(to='blog.Category', verbose_name='Категория')),
            ],
            options={
                'ordering': ['published_date'],
            },
        ),
        migrations.AlterIndexTogether(
            name='post',
            index_together={('title', 'slug')},
        ),
    ]

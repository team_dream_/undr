from django.shortcuts import get_object_or_404
from .models import Post, Category
from django.views import generic


class ViewMainPage(generic.ListView):
    template_name = 'pages/main_page_blog.html'
    context_object_name = 'posts'
    paginate_by = 10

    def get_queryset(self):
        self.queryset_ = Post.objects.all()
        return self.queryset_

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['categories'] = Category.objects.all()
        context['show_paginate'] = True if self.queryset_.count() > self.paginate_by else False
        return context


class ViewCategoryPage(generic.ListView):
    model = Post
    template_name = 'pages/page_filter_categories.html'
    context_object_name = "posts"
    paginate_by = 10

    def get_queryset(self):
        self.category = get_object_or_404(Category, slug=self.kwargs['category_slug'])
        return Post.objects.filter(categories=self.category)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['category'] = self.category
        return context


class ViewPostPage(generic.ListView):
    model = Post
    template_name = 'pages/page_post.html'
    context_object_name = "post"

    def get_queryset(self):
        post = get_object_or_404(Post, slug=self.kwargs['slug'])
        return post
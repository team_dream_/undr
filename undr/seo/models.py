from django.db import models


class SeoModel(models.Model):
    seo_title = models.CharField('Title', blank=True, max_length=250)
    seo_description = models.CharField('Description', blank=True, max_length=250)
    seo_keywords = models.CharField('Keywords', blank=True, max_length=250)

    def blank_seo(self):
        return False

    def get_seo_title(self):
        if self.seo_title:
            return self.seo_title
        return self.blank_seo()

    def get_seo_description(self):
        if self.seo_description:
            return self.seo_description

    def get_seo_keywords(self):
        if self.seo_keywords:
            return self.seo_keywords

    class Meta:
        abstract = True
from django.db import models


class Game(models.Model):
    price = models.CharField('Цена', max_length=50)
    old_price = models.CharField('Старая цена', max_length=50)
    discount = models.CharField('Скидка', max_length=50)
    title = models.CharField('Название', max_length=250)
    platform = models.TextField('Платформа', max_length=50)
    link = models.CharField('Ссылка', max_length=300)
    image = models.ImageField('Изображение', blank=True, upload_to='media', max_length=200)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'game'
        ordering = ['-discount']
        verbose_name = 'Игра'
        verbose_name_plural = 'Игры'
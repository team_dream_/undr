from w3lib.html import replace_escape_chars

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join, Identity

from undr_spider.items import MvideoSpiderItem

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class QuotesSpider(CrawlSpider):
    name = "mvideo"


    def start_requests(self):
        urls = [
            'https://www.mvideo.ru/playstation/ps4-igry-4331/',
            'https://www.mvideo.ru/xbox/xbox-one-igry-4338/',
            'https://www.mvideo.ru/playstation/ps3-igry-4332',
            'https://www.mvideo.ru/xbox/xbox-360-igry-170',
            'https://www.mvideo.ru/nintendo/nintendo-igry-4929',
            'https://www.mvideo.ru/pc-igry-i-soft/pc-igry-91'
        ]
        rules = [
            Rule(LinkExtractor(allow=r'questions\?page=[0-9]&sort=newest'),
                 callback='parse_item', follow=True)
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.get_url_for_categories)

    def get_url_for_categories(self, response):
        sale_categories = response.css('#Promotion-section label a::attr(href)').extract()[:-1]
        for category in sale_categories:
            yield scrapy.Request(url=response.url + category, callback=self.to_page)

    def to_page(self, response):
        pages = response.css('div.c-pagination.notranslate div.c-toggle-buttons a.c-pagination__num.c-btn.'
                             'c-btn_white.c-toggle-buttons__item::attr(href)').extract()
        for page in pages:
            yield scrapy.Request(url=response.urljoin(page), callback=self.parse)

    def parse(self, response):
        games = response.css('div.c-product-tile.sel-product-tile-main')
        for game in games:
            mvideo = MvideoSpiderItem()
            mvideo['price'] = ''.join(game.css('div.c-pdp-price__current::text').re('[\d]+'))
            mvideo['old_price'] = ''.join(
                game.css('div.c-pdp-price__sale span.u-mr-4.c-pdp-price__old::text').re('[\d]+'))
            mvideo['discount'] = ''.join(game.css('div.c-pdp-price__sale span.c-pdp-price__discount::text').re('[\d]+'))
            mvideo['title'] = game.css('h4::attr(title)').extract_first()
            mvideo['platform'] = 'PS4'
            mvideo['link'] = 'https://www.mvideo.ru' + \
                   game.css('div.c-product-tile.sel-product-tile-main div.c-product-tile-picture__holder a::attr(href)'
                            ).extract()[0]
            mvideo['image_urls'] = ['https:' + game.css(
                'img.lazy.product-tile-picture__image::attr(data-original)').extract_first()]

            yield mvideo
# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class MvideoSpiderItem(scrapy.Item):
    price = scrapy.Field()
    old_price = scrapy.Field()
    discount = scrapy.Field()
    title = scrapy.Field()
    platform = scrapy.Field()
    link = scrapy.Field()
    image_paths = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
